package com.fibonacci.yuriikoval1997;

/**
 * It's a final class with one static method which allows to process given range.
 * @author Yurii Koval
 */
final class Ranges {
    static void processAndPrintRange(int beginning, int end){
        if (beginning > end){
            throw new IllegalArgumentException("Invalid range");
        }
        int beginningOdd = beginning;
        int endEven = end;
        int sumOdd = 0;
        int sumEven = 0;
        if (beginning % 2 == 0){
            beginningOdd++;
        }
        if (end % 2 == 1){
            endEven--;
        }
        System.out.print("Here is odd numbers of the range in ascending order: ");
        for (int i = beginningOdd; i < end; i +=2) {
            sumOdd +=i;
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println("The sum of the odd numbers is: " + sumOdd);
        System.out.println();
        System.out.print("Here is even numbers of the range in descending order: ");
        for (int i = endEven; i >= beginning; i -=2){
            sumEven +=i;
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println("The sum of the even numbers is: " + sumEven);
    }
}
