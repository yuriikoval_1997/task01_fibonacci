package com.fibonacci.yuriikoval1997;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Starter {
    public static void main(String[] args) throws IOException {
        try(BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))){
            askRange(consoleReader);
            askFibNum(consoleReader);
        }
    }

    private static void askRange(BufferedReader consoleReader) throws IOException {
        int beginning;
        int end;
        while (true){
            try {
                System.out.print("Enter the beginning of the range, it has to be Natural number: ");
                beginning = Integer.parseInt(consoleReader.readLine());
                System.out.print("Enter the end of the range, it has to be Natural and bigger than the beginning: ");
                end = Integer.parseInt(consoleReader.readLine());
                System.out.println();
            } catch (NumberFormatException e){
                System.out.println("You have entered an invalid number. Please retry!");
                System.out.println();
                continue;
            }
            if (beginning > 0 && beginning < end){
                break;
            } else {
                System.out.println("You have entered invalid range. Please retry!");
                System.out.println();
            }
        }
        Ranges.processAndPrintRange(beginning, end);
    }

    private static void askFibNum(BufferedReader consoleReader) throws IOException {
        System.out.println();
        System.out.println("Do you want to find the value of a fibonacci number? If yes enter a Natural number, " +
                "else enter any letter.");
        int num;
        while (true){
            try {
                num = Integer.parseInt(consoleReader.readLine());
            } catch (NumberFormatException ignored){
                System.out.println("We hope you have enjoyed using this program!");
                return;
            }
            if (num <= 0){
                System.out.println("You entered not a Natural number. Please retry!");
                continue;
            }
            try (FibonacciNumber fibonacciNumber = new FibonacciNumber(num)){
                System.out.printf("(Solved with a loop) The %d-s fibonacci number is %d",num, fibonacciNumber.findFibNumWithLoop());
                System.out.println();
                System.out.printf("(Solved with a recursion) The %d-s fibonacci number is %d",num,
                        fibonacciNumber.findFibNumWithRecursion());
                System.out.println();
                System.out.println("Percentage of even numbers: " + fibonacciNumber.findPercentageOfEvenNumbers());
                System.out.println("Percentage of odd numbers: "+ fibonacciNumber.findPercentageOfOddNumbers());
                System.out.println("We hope you have enjoyed using this program!");
            }
            break;
        }
    }
}
