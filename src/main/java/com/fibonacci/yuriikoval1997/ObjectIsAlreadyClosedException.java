package com.fibonacci.yuriikoval1997;

public class ObjectIsAlreadyClosedException extends RuntimeException {
    public ObjectIsAlreadyClosedException(){
        super("This object has already been closed.");
    }
}
