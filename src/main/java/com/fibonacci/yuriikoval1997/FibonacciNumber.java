package com.fibonacci.yuriikoval1997;

/**
 * This class is used to find value of a fibonacci number
 */
class FibonacciNumber implements AutoCloseable{
    private boolean isClosed;
    private int[] fibArr;
    private int numberOfEvenNumbers;

    // Takes fibonacci number and creates corresponding array
    FibonacciNumber(int num){
        fibArr = new int[num + 1];
    }

    /**
     * calculates fibonacci number with a loop and fills the array
     * @return fibonacci number
     */
    int findFibNumWithLoop() throws ObjectIsAlreadyClosedException{
        if (isClosed) {
            throw new ObjectIsAlreadyClosedException();
        }
        int num = fibArr.length;
        if (num == 0) {
            return 0;
        }
        if (num == 1 || num == 2) {
            return 1;
        }
        int previous = 1;
        int current = 1;
        fibArr[1] = previous;
        fibArr[2] = current;
        for (int i = 3; i < num; i++) {
            int temp = previous;
            previous = current;
            current = previous + temp;
            fibArr[i] = current;
            if (current % 2 == 0){
                numberOfEvenNumbers++;
            }
        }
        return fibArr[num - 1];
    }

    /**
     * calculates fibonacci number with a loop and fills the array
     * @return fibonacci number
     */
    int findFibNumWithRecursion() throws ObjectIsAlreadyClosedException{
        if (isClosed) {
            throw new ObjectIsAlreadyClosedException();
        }
        return recursion(fibArr.length - 1);
    }

    private int recursion(int n) {
        if (n == 1){
            fibArr[1] = 1;
            return 1;
        }
        if (n == 2){
            fibArr[2] = 1;
            return 1;
        }
        if (fibArr[n] != 0){
            if (fibArr[n] % 2 == 0){
                numberOfEvenNumbers++;
            }
            return fibArr[n];
        }
        fibArr[n - 1] = recursion(n - 1);
        fibArr[n - 2] = recursion(n - 2);
        return fibArr[n - 1] + fibArr[n - 2];
    }

    double findPercentageOfEvenNumbers() throws ObjectIsAlreadyClosedException{
        if (isClosed) {
            throw new ObjectIsAlreadyClosedException();
        }
        return (double) numberOfEvenNumbers / (fibArr.length - 1);
    }

    double findPercentageOfOddNumbers() throws ObjectIsAlreadyClosedException{
        if (isClosed) {
            throw new ObjectIsAlreadyClosedException();
        }
        return 1.0 - findPercentageOfEvenNumbers();
    }

    @Override
    public void close() throws ObjectIsAlreadyClosedException {
        if (isClosed) {
            throw new ObjectIsAlreadyClosedException();
        }
        this.fibArr = null;
        isClosed = true;
    }
}
